﻿using RDP.Business.Base;

namespace RDP.Business.Interfaces
{
    public interface IBusinessDUSS: IBUGeneric
    {
        void MethodE();

        void MethodF();
    }
}
