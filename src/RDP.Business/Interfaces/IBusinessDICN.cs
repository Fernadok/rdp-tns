﻿using RDP.Business.Base;
using RDP.Domain;

namespace RDP.Business.Interfaces
{
    public abstract class BaseBusinessDICN : BaseBusiness<Order, Order, Order, Order>
    {
        public abstract void MethodA();

        public abstract void MethodB();
    }
}
