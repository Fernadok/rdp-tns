﻿using RDP.Business.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Business.Interfaces
{
    public interface IBusinessDUSN: IBUGeneric
    {
        void MethodC();

        void MethodD();
    }
}
