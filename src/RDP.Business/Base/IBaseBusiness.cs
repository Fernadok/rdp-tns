﻿using System.Collections.Generic;

namespace RDP.Business.Base
{
    public interface IBaseBusiness<AppData, AppDataFixedFilters, AppDataLocalFilters, AppDataUpsert>
           where AppData : new()
           where AppDataFixedFilters : new()
           where AppDataLocalFilters : new()
           where AppDataUpsert : new()
    {
        AppData GetById(int id);

        List<AppData> SearchAll();

        List<AppData> Search(AppDataFixedFilters request);

        List<AppData> SearchWithLocalFilters(AppDataFixedFilters fixedFilters, AppDataLocalFilters localFilters);

        List<AppData> SearchAllById(int id, AppDataLocalFilters filter);

        int Insert(AppDataUpsert request);

        int Update(AppDataUpsert request);

        int Delete(AppData request);
    }
}
