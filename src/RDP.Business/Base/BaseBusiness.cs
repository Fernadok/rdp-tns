﻿using System;
using System.Collections.Generic;

namespace RDP.Business.Base
{
    public class BaseBusiness<AppData, AppDataFixedFilters, AppDataLocalFilters, AppDataUpsert> : IBaseBusiness<AppData, AppDataFixedFilters, AppDataLocalFilters, AppDataUpsert>
            where AppData : new()
            where AppDataFixedFilters : new()
            where AppDataLocalFilters : new()
            where AppDataUpsert : new()
    {
        public int Delete(AppData request)
        {
            throw new NotImplementedException();
        }

        public AppData GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int Insert(AppDataUpsert request)
        {
            throw new NotImplementedException();
        }

        public List<AppData> Search(AppDataFixedFilters request)
        {
            throw new NotImplementedException();
        }

        public List<AppData> SearchAll()
        {
            throw new NotImplementedException();
        }

        public List<AppData> SearchAllById(int id, AppDataLocalFilters filter)
        {
            throw new NotImplementedException();
        }

        public List<AppData> SearchWithLocalFilters(AppDataFixedFilters fixedFilters, AppDataLocalFilters localFilters)
        {
            throw new NotImplementedException();
        }

        public int Update(AppDataUpsert request)
        {
            throw new NotImplementedException();
        }
    }
}
