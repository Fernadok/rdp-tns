﻿using Abp.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace RDP.Extensions
{
    public static class StringExt
    {
        public static string Localize(this string key)
        {
            return LocalizationHelper.GetString(RDPConsts.LocalizationSourceName, key);
        }

        public static bool ValidateJSON(this string s)
        {
            try
            {
                JToken.Parse(s);
                return true;
            }
            catch (JsonReaderException ex)
            {
                Trace.WriteLine(ex);
                return false;
            }
        }

    }
}
