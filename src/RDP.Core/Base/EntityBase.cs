﻿using Abp.Domain.Entities;
using System;
using Abp.Localization;
using RDP.Extensions;

namespace RDP.Base
{
    public abstract class EntityBase<TPrimaryKey> : Entity<TPrimaryKey>, IEntityBase<TPrimaryKey>
    {
        public EntityBase()
        {
            IsActive = true;
            IsDeleted = false;
            DateCreated = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
        }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateCreatedLocal => DateCreated.ToLocalTime();

        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }

        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public DateTime? LastModificationTimeLocal => LastModificationTime?.ToLocalTime();

        public string L(string key)
        {
            return key.Localize();
        }
    }

    public abstract class EntityBase : EntityBase<long>
    {
        public EntityBase()
        {
            //Id = NewLong();
        }

        public long NewLong()
        {
            var dateunique = Convert.ToInt64(DateTime.Now.ToString("yyyymmddhhss"));
            return dateunique;
        }
    }
}
