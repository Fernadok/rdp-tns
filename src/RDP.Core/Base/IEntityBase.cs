﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Base
{
    public interface IEntityBase<TPrimaryKey> : IEntity<TPrimaryKey>, IDeletionAudited, IModificationAudited
    {
        bool IsActive { get; set; }
        DateTime DateCreatedLocal { get; }
        DateTime? LastModificationTimeLocal { get; }
    }
}
