﻿using Abp.Authorization;
using RDP.Authorization.Roles;
using RDP.Authorization.Users;

namespace RDP.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
