﻿namespace RDP.Providers
{
    public interface INetWorkProvider
    {
        string MapPath(string path);
        string GetAppSettings(string key);
        bool IsProductionEnv();
        string GetConnetionString();
    }
}
