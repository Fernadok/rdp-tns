﻿using RDP.Authorization.Users;
using RDP.Base;
using RDP.Enums;
using System.Collections.Generic;

namespace RDP.Domain
{
    public class Order : EntityBase
    {
        public Order()
        {
            OrderDetails = new List<OrderDetail>();
        }

        public long? CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public long UserId { get; set; }
        public virtual User User { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }
    }
}
