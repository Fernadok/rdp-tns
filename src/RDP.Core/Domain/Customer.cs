﻿using RDP.Authorization.Users;
using RDP.Base;
using System.Collections.Generic;

namespace RDP.Domain
{
    public class Customer : EntityBase
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }

        public long SubBussinessUnitId { get; set; }
        public virtual SubBussinessUnit SubBussinessUnit { get; set; }

        public long UserId { get; set; }
        public virtual User User { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }


            public virtual HashSet<Order> Orders { get; set; }

    }
}
