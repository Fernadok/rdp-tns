﻿using RDP.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Domain
{
    public class BussinessUnit : EntityBase
    {
        public BussinessUnit()
        {
            SubBussinessUnits = new HashSet<SubBussinessUnit>();
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public virtual HashSet<SubBussinessUnit> SubBussinessUnits { get; set; }

    }
}
