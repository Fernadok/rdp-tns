﻿using RDP.Base;
using System.Collections.Generic;

namespace RDP.Domain
{
    public class SubBussinessUnit : EntityBase
    {
        public SubBussinessUnit()
        {
            Customers = new HashSet<Customer>();
        }

        public long BussinessUnitId { get; set; }
        public virtual BussinessUnit BussinessUnit { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public virtual HashSet<Customer> Customers { get; set; }

    }
}
