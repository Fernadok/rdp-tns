﻿using RDP.Base;

namespace RDP.Domain
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public long Quantity { get; set; }

    }
}
