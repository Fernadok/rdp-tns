﻿using RDP.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Domain
{
    public class OrderDetail : EntityBase
    {
        public long OrderId { get; set; }
        public virtual Order Order { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public long Quantity { get; set; }
    }
}
