﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Enums
{
    public enum OrderStatus
    {
        Proccessing = 1,
        Accepted = 2,
        Canceled = 3
    }
}
