﻿namespace RDP
{
    public class RDPConsts
    {
        public const string LocalizationSourceName = "RDP";

        public const bool MultiTenancyEnabled = true;
    }
}