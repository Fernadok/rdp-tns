﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace RDP.EntityFramework.Repositories
{
    public abstract class RDPRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<RDPDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected RDPRepositoryBase(IDbContextProvider<RDPDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class RDPRepositoryBase<TEntity> : RDPRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected RDPRepositoryBase(IDbContextProvider<RDPDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
