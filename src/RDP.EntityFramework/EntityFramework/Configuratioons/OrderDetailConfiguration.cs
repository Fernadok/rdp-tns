﻿using RDP.Domain;

namespace RDP.EntityFramework.Configuratioons
{
    public class OrderDetailConfiguration : EntityConfiguration<OrderDetail>
    {
        public OrderDetailConfiguration()
        {
            HasRequired(x => x.Order)
                .WithMany(x => x.OrderDetails)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
