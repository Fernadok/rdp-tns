﻿using RDP.Domain;

namespace RDP.EntityFramework.Configuratioons
{
    public class CustomerConfiguration : EntityConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            HasRequired(x => x.SubBussinessUnit)
                    .WithMany(x => x.Customers)
                    .HasForeignKey(x => x.SubBussinessUnitId);

            HasRequired(x => x.User)
                    .WithMany(x => x.Customers)
                    .HasForeignKey(x => x.UserId);

            HasIndex(x => x.SubBussinessUnitId);
            HasIndex(x => x.UserId);

        }
    }
}
