﻿using RDP.Domain;

namespace RDP.EntityFramework.Configuratioons
{
    public class SubBussinessUnitConfiguration : EntityConfiguration<SubBussinessUnit>
    {
        public SubBussinessUnitConfiguration()
        {
            HasRequired(x => x.BussinessUnit)
                 .WithMany(x => x.SubBussinessUnits)
                 .HasForeignKey(x => x.BussinessUnitId);

            HasIndex(x => x.BussinessUnitId);
        }
    }
}
