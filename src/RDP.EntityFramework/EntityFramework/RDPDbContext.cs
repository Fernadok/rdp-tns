﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Abp.DynamicEntityParameters;
using Abp.Zero.EntityFramework;
using RDP.Authorization.Roles;
using RDP.Authorization.Users;
using RDP.Domain;
using RDP.EntityFramework.Configuratioons;
using RDP.MultiTenancy;

namespace RDP.EntityFramework
{
    public class RDPDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public virtual IDbSet<BussinessUnit> BussinessUnits { get; set; }
        public virtual IDbSet<SubBussinessUnit> SubBussinessUnits { get; set; }
        public virtual IDbSet<Customer> Customers { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<OrderDetail> OrderDetails { get; set; }
        public virtual IDbSet<Product> Products { get; set; }

        public RDPDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in RDPDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of RDPDbContext since ABP automatically handles it.
         */
        public RDPDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public RDPDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public RDPDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new BussinessUnitConfiguration());
            modelBuilder.Configurations.Add(new SubBussinessUnitConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new OrderDetailConfiguration());

            base.OnModelCreating(modelBuilder);
        
            modelBuilder.Entity<DynamicParameter>().Property(p => p.ParameterName).HasMaxLength(250);
            modelBuilder.Entity<EntityDynamicParameter>().Property(p => p.EntityFullName).HasMaxLength(250);
        }
    }
}
