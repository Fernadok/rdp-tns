﻿using RDP.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.EntityFramework
{
    public class EntityConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntityBase<long>
    {
        public EntityConfiguration()
        {
            HasKey(x => x.Id);
            Ignore(x => x.DateCreatedLocal);
            Ignore(x => x.LastModificationTimeLocal);
        }
    }
}
