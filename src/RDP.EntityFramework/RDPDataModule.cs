﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using RDP.EntityFramework;

namespace RDP
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(RDPCoreModule))]
    public class RDPDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<RDPDbContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RDPDbContext, Migrations.Configuration>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
