﻿namespace RDP.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AbpDynamicParameterValues", "DynamicParameterId", "dbo.AbpDynamicParameters");
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.AbpEntityPropertyChanges", "EntityChangeId", "dbo.AbpEntityChanges");
            DropForeignKey("dbo.AbpEntityChanges", "EntityChangeSetId", "dbo.AbpEntityChangeSets");
            DropForeignKey("dbo.AbpEntityDynamicParameters", "DynamicParameterId", "dbo.AbpDynamicParameters");
            DropForeignKey("dbo.AbpEntityDynamicParameterValues", "EntityDynamicParameterId", "dbo.AbpEntityDynamicParameters");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpWebhookSendAttempts", "WebhookEventId", "dbo.AbpWebhookEvents");
            CreateTable(
                "dbo.BussinessUnits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BussinessUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.SubBussinessUnits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BussinessUnitId = c.Long(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubBussinessUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BussinessUnits", t => t.BussinessUnitId)
                .Index(t => t.BussinessUnitId)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SubBussinessUnitId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        Name = c.String(),
                        Address = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Customer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubBussinessUnits", t => t.SubBussinessUnitId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.SubBussinessUnitId)
                .Index(t => t.UserId)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CustomerId = c.Long(),
                        OrderStatus = c.Int(nullable: false),
                        UserId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Order_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .Index(t => t.CustomerId)
                .Index(t => t.UserId)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OrderId = c.Long(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Model = c.String(),
                        Quantity = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .Index(t => t.OrderId)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Model = c.String(),
                        Quantity = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Product_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
            AddForeignKey("dbo.AbpDynamicParameterValues", "DynamicParameterId", "dbo.AbpDynamicParameters", "Id");
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id");
            AddForeignKey("dbo.AbpEntityPropertyChanges", "EntityChangeId", "dbo.AbpEntityChanges", "Id");
            AddForeignKey("dbo.AbpEntityChanges", "EntityChangeSetId", "dbo.AbpEntityChangeSets", "Id");
            AddForeignKey("dbo.AbpEntityDynamicParameters", "DynamicParameterId", "dbo.AbpDynamicParameters", "Id");
            AddForeignKey("dbo.AbpEntityDynamicParameterValues", "EntityDynamicParameterId", "dbo.AbpEntityDynamicParameters", "Id");
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id");
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.AbpWebhookSendAttempts", "WebhookEventId", "dbo.AbpWebhookEvents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbpWebhookSendAttempts", "WebhookEventId", "dbo.AbpWebhookEvents");
            DropForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.AbpEntityDynamicParameterValues", "EntityDynamicParameterId", "dbo.AbpEntityDynamicParameters");
            DropForeignKey("dbo.AbpEntityDynamicParameters", "DynamicParameterId", "dbo.AbpDynamicParameters");
            DropForeignKey("dbo.AbpEntityChanges", "EntityChangeSetId", "dbo.AbpEntityChangeSets");
            DropForeignKey("dbo.AbpEntityPropertyChanges", "EntityChangeId", "dbo.AbpEntityChanges");
            DropForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions");
            DropForeignKey("dbo.AbpDynamicParameterValues", "DynamicParameterId", "dbo.AbpDynamicParameters");
            DropForeignKey("dbo.Customers", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.Customers", "SubBussinessUnitId", "dbo.SubBussinessUnits");
            DropForeignKey("dbo.Orders", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.SubBussinessUnits", "BussinessUnitId", "dbo.BussinessUnits");
            DropIndex("dbo.Products", new[] { "IsDeleted" });
            DropIndex("dbo.OrderDetails", new[] { "IsDeleted" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.Orders", new[] { "IsDeleted" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.Customers", new[] { "IsDeleted" });
            DropIndex("dbo.Customers", new[] { "UserId" });
            DropIndex("dbo.Customers", new[] { "SubBussinessUnitId" });
            DropIndex("dbo.SubBussinessUnits", new[] { "IsDeleted" });
            DropIndex("dbo.SubBussinessUnits", new[] { "BussinessUnitId" });
            DropIndex("dbo.BussinessUnits", new[] { "IsDeleted" });
            DropTable("dbo.Products",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Product_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.OrderDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Orders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Order_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Customers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Customer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SubBussinessUnits",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SubBussinessUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.BussinessUnits",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BussinessUnit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            AddForeignKey("dbo.AbpWebhookSendAttempts", "WebhookEventId", "dbo.AbpWebhookEvents", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserRoles", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserLogins", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpUserClaims", "UserId", "dbo.AbpUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpPermissions", "RoleId", "dbo.AbpRoles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpEntityDynamicParameterValues", "EntityDynamicParameterId", "dbo.AbpEntityDynamicParameters", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpEntityDynamicParameters", "DynamicParameterId", "dbo.AbpDynamicParameters", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpEntityChanges", "EntityChangeSetId", "dbo.AbpEntityChangeSets", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpEntityPropertyChanges", "EntityChangeId", "dbo.AbpEntityChanges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpFeatures", "EditionId", "dbo.AbpEditions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AbpDynamicParameterValues", "DynamicParameterId", "dbo.AbpDynamicParameters", "Id", cascadeDelete: true);
        }
    }
}
