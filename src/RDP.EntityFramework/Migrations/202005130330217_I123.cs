﻿namespace RDP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class I123 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            CreateIndex("dbo.Orders", "CustomerId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            CreateIndex("dbo.Orders", "CustomerId");
        }
    }
}
