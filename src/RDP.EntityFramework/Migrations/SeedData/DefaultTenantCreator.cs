using System.Linq;
using RDP.EntityFramework;
using RDP.MultiTenancy;

namespace RDP.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly RDPDbContext _context;

        public DefaultTenantCreator(RDPDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
