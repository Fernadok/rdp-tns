﻿

using Abp.WebApi.Controllers;
using Newtonsoft.Json;
using RDP.Extensions;
using RDP.Orders.Dto;
using RDP.Products;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace RDP.Api.Controllers
{
    public class OrderController : AbpApiController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet()]
        public IHttpActionResult GetOrder(long id)
        {
            var order = _orderService.GetById(id).FirstOrDefault();

            var ignore = new HashSet<string> { "OrderId" };
            var rename = new Dictionary<string, string>() { { "Quantity", "QuantityChanged" } };

            var jsonResolver = new PropertySerializer<OrderDataListDto>(ignore, rename);

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = jsonResolver
            };

            var json = JsonConvert.SerializeObject(order, serializerSettings);
            return Ok(new { results = json });
        }
    }
}
