﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using RDP.Authorization.Users;
using RDP.MultiTenancy;
using RDP.Users;
using Microsoft.AspNet.Identity;
using RDP.Providers;
using Abp.Domain.Repositories;

namespace RDP
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class RDPAppServiceBase : ApplicationService
    {
        public IRepository<User, long> UserRepository { get; set; }

        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }
  
        protected RDPAppServiceBase()
        {
            LocalizationSourceName = RDPConsts.LocalizationSourceName;
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected virtual User GetCurrentUser()
        {
            if (!AbpSession.UserId.HasValue) { return null; }
            var user = UserRepository.FirstOrDefault(x => x.Id == AbpSession.UserId.Value);
            if (user == null)
            {
                return null;
            }

            return user;
        }
    }
}