﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RDP.Sessions.Dto;

namespace RDP.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
        Task<GetCurrentLoginInformationsOutput> GetCurrentLogin();

    }
}
