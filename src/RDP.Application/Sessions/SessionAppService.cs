﻿using System.IO;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.AutoMapper;
using Abp.ObjectMapping;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RDP.Providers;
using RDP.Sessions.Dto;

namespace RDP.Sessions
{
    public class SessionAppService : RDPAppServiceBase, ISessionAppService
    {
        public readonly INetWorkProvider _netWorkProvider;

        private readonly IObjectMapper _objectMapper;

        public SessionAppService(IObjectMapper objectMapper, INetWorkProvider netWorkProvider)
        {
            _objectMapper = objectMapper;
            _netWorkProvider = netWorkProvider;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput();

            if (AbpSession.UserId.HasValue)
            {
                output.User = _objectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
            }

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = _objectMapper.Map<TenantLoginInfoDto>(await GetCurrentTenantAsync());
            }
      
            return output;
        }


        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLogin()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                User = GetCurrentUser().MapTo<UserLoginInfoDto>()
            };

            string p = _netWorkProvider.MapPath($"/Views/Configurations/grid-definition-{output.User.UserName}.json");

            output.User.Configurations = JObject.Parse(File.ReadAllText(p));


            return output;
        }
    }
}