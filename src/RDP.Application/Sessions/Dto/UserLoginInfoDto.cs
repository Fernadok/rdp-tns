﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Newtonsoft.Json.Linq;
using RDP.Authorization.Users;
using RDP.Users;

namespace RDP.Sessions.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserLoginInfoDto : EntityDto<long>
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public JObject Configurations { get; set; }
    }
}
