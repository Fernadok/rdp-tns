﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using NinjaNye.SearchExtensions;
using PagedList;
using RDP.Authorization;
using RDP.Base;
using RDP.Domain;
using RDP.Extensions;
using RDP.Products.Dto;
using System.Collections.Generic;
using System.Linq;

namespace RDP.Products
{
    [AbpAuthorize(PermissionNames.Pages_Users, PermissionNames.Pages_Orders)]
    public class ProductService : RDPAppServiceBase, IProductService
    {
        private readonly IRepository<Product, long> _productRepository;

        public ProductService(IRepository<Product, long> productRepository)
        {
            _productRepository = productRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(ProductoDto dto)
        {
            var isnew = _productRepository.FirstOrDefault(x => x.Id == dto.Id);
            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Name.ToLower() != dto.Name.ToLower())
            {
                var existother = _productRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un profucto con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = _productRepository.FirstOrDefault(x => x.Name.ToLower() == dto.Name.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un producto con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            var ar = dto.MapTo(isnew);
            _productRepository.InsertOrUpdate(ar);
            CurrentUnitOfWork.SaveChanges();

            return ar.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var area = _productRepository.FirstOrDefault(x => x.Id == id);
            if (area != null)
            {
                area.IsActive = false;
                _productRepository.Update(area);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<ProductoDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _productRepository
                                .GetAll()
                                .Where(x => x.IsActive)
                                .Search(x => x.Name)
                                .Containing(filters.Search)
                                .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                                .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<ProductoDto>
            {
                Data = resultList.Select(x => x.MapTo<ProductoDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public ProductoDto GetById(long id)
        {
            var produvt = _productRepository.FirstOrDefault(x => x.Id == id);
            return produvt?.MapTo<ProductoDto>();
        }

        public List<IdDescription> ProductList()
        {
            return _productRepository.GetAll()
                    .Where(x => x.IsActive)
                    .Select(x => new IdDescription
                    {
                        Id = x.Id,
                        Description = x.Name
                    }).OrderBy(x => x.Description).ToList();
        }
    }
}
