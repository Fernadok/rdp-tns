﻿using Abp.Application.Services;
using RDP.Base;
using RDP.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Products
{
    public interface IProductService:IApplicationService
    {
        ContextDto<ProductoDto> GetAll(SearchDto filters);
       
        long AddOrUpdate(ProductoDto dto);

        void Delete(long id);
      
        ProductoDto GetById(long id);

        List<IdDescription> ProductList();
    }
}
