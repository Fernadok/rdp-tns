﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using RDP.Domain;

namespace RDP.Products.Dto
{
    [AutoMap(typeof(Product))]
    public class ProductoDto : Entity<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public long Quantity { get; set; }
    }
}
