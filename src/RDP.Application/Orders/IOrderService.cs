﻿using Abp.Application.Services;
using RDP.Base;
using RDP.Orders.Dto;
using System.Collections.Generic;

namespace RDP.Products
{
    public interface IOrderService : IApplicationService
    {
        ContextDto<OrderDataListDto> GetAll(SearchDto filters);

        long AddOrUpdate(OrderDto dto);

        void Delete(long id);

        List<OrderDataListDto> GetById(long id);

        List<IdDescription> OrderList();
    }
}
