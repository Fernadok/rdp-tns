﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using RDP.Domain;

namespace RDP.Orders.Dto
{
    [AutoMap(typeof(OrderDetail))]
    public class OrderDetailDto : Entity<long>
    {
        public long OrderId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public long Quantity { get; set; }
    }
}
