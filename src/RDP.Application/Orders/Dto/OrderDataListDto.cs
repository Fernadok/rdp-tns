﻿using RDP.Enums;

namespace RDP.Orders.Dto
{
    public class OrderDataListDto
    {
        /// <summary>
        /// Order identity.
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// Customer Identity.
        /// </summary>
        public long? CustomerId { get; set; }

        /// <summary>
        /// Order status.
        /// </summary>
        public OrderStatus OrderStatus { get; set; }

        /// <summary>
        /// Order status name.
        /// </summary>
        public string OrderStatusName => OrderStatus.ToString();
 
        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Product model.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Item quantity.
        /// </summary>
        public long Quantity { get; set; }
    }
}
