﻿using Abp.Domain.Entities;
using AutoMapper;
using RDP.Authorization.Users;
using RDP.Domain;
using RDP.Enums;
using RDP.Users.Dto;
using System.Collections.Generic;

namespace RDP.Orders.Dto
{
    public class OrderDto : Entity<long>
    {
        public OrderDto()
        {
            OrderDetails = new List<OrderDetailDto>();
        }

        public long? CustomerId { get; set; }
 
        public OrderStatus OrderStatus { get; set; }

        public string OrderStatusName => OrderStatus.ToString();

        public List<OrderDetailDto> OrderDetails { get; set; }

    }
}
