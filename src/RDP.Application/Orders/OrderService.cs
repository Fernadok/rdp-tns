﻿

using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using AutoMapper.Mappers;
using Castle.DynamicProxy.Generators;
using NinjaNye.SearchExtensions;
using PagedList;
using RDP.Base;
using RDP.Domain;
using RDP.Extensions;
using RDP.Orders.Dto;
using RDP.Products;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RDP.Orders
{
    public class OrderService : RDPAppServiceBase, IOrderService
    {
        private readonly IRepository<Order, long> _orderRepository;

        public OrderService(IRepository<Order, long> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(OrderDto dto)
        {
            var order = new Order
            {
                UserId = AbpSession.UserId.Value,
                OrderStatus = dto.OrderStatus,

                OrderDetails = dto.OrderDetails.Select(s => s.MapTo<OrderDetail>()).ToList(),

                DateCreated = DateTime.UtcNow,
                IsActive = true,
                IsDeleted = false
            };

            _orderRepository.InsertOrUpdate(order);
            CurrentUnitOfWork.SaveChanges();

            return order.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var order = _orderRepository.FirstOrDefault(x => x.Id == id);
            if (order != null)
            {
                order.IsActive = false;
                _orderRepository.Update(order);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<OrderDataListDto> GetAll(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = _orderRepository
                                .GetAllIncluding(i => i.User, i => i.OrderDetails)
                                .Where(x => x.IsActive && x.UserId == AbpSession.UserId.Value)
                                .Search(x => x.Id.ToString())
                                .Containing(filters.Search)
                                .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                                .ToPagedList(filters.Page, filters.PageSize);


            var OrderDataList = new List<OrderDataListDto>();
            foreach (var item in resultList)
            {
                foreach (var row in item.OrderDetails)
                {
                    OrderDataList.Add(new OrderDataListDto { 
                        CustomerId= item.CustomerId,
                        OrderId = item.Id,
                        OrderStatus = item.OrderStatus,
                        Name= row.Name,
                        Description = row.Description,
                        Model= row.Model,
                        Quantity = row.Quantity
                    });
                }
            }

            return new ContextDto<OrderDataListDto>
            {
                Data = OrderDataList,
                MetaData = resultList.GetMetaData()
            };
        }

        public List<OrderDataListDto> GetById(long id)
        {
            var order = _orderRepository.GetAllIncluding(x=>x.OrderDetails).FirstOrDefault(x => x.Id == id);
           
            var OrderDataList = new List<OrderDataListDto>();
            foreach (var row in order.OrderDetails)
            {
                OrderDataList.Add(new OrderDataListDto
                {
                    CustomerId = order.CustomerId,
                    OrderId = order.Id,
                    OrderStatus = order.OrderStatus,
                    Name = row.Name,
                    Description = row.Description,
                    Model = row.Model,
                    Quantity = row.Quantity
                });
            }

            return OrderDataList;
        }

        public List<IdDescription> OrderList()
        {
            return _orderRepository.GetAll()
                    .Where(x => x.IsActive)
                    .Select(x => new IdDescription
                    {
                        Id = x.Id,
                        Description = x.OrderStatus.ToString()
                    }).OrderBy(x => x.Description).ToList();
        }
    }
}
