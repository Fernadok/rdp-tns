﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using RDP.Configuration.Dto;

namespace RDP.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : RDPAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
