using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RDP.Roles.Dto;
using RDP.Users.Dto;

namespace RDP.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}