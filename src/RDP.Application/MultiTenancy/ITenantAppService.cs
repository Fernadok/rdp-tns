﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using RDP.MultiTenancy.Dto;

namespace RDP.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
