﻿using System.Threading.Tasks;
using Abp.Application.Services;
using RDP.Authorization.Accounts.Dto;

namespace RDP.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
