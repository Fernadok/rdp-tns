﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDP.Base
{
    public class SearchDto
    {
        public SearchDto()
        {
            Page = 1;
            PageSize = 10;
            OrderColumn = "Id";
            Order = "desc";
        }

        public long Id { get; set; }
        public string Search { get; set; }
        public int Page { get; set; }
        public string Order { get; set; }
        public int PageSize { get; set; }
        public string OrderColumn { get; set; }

        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }

    }
}
