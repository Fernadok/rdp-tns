﻿using PagedList;
using System.Collections.Generic;

namespace RDP.Base
{
    public class ContextDto<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }
        public IPagedList MetaData { get; set; }
    }
}
