﻿using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RDP.Web.Helpers
{
    public static class GeneralHelpers
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            return IsDebug();
        }

        public static bool IsDebug()
        {
            var dev = ConfigurationManager.AppSettings["Env"].ToString();
            return !dev.IsNullOrEmpty() && dev == "DEV";
        }

        public static byte[] ToBytes(this HttpPostedFileBase file)
        {
            byte[] data;
            using (Stream inputStream = file.InputStream)
            {
                if (!(inputStream is MemoryStream memoryStream))
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
            }
            return data;
        }
    }
}