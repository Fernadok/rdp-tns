﻿var abp = abp || {};
(function () {

    if (!$.blockUI) {
        return;
    }

    $.extend($.blockUI.defaults, {
        message: ' ',
        css: {},
        overlayCSS: {
            backgroundColor: 'rgb(69, 23, 90)',
            opacity: 0.3,
            cursor: 'wait'
        }
    });

    abp.ui.icon = null;
    abp.ui.block = function (elm) {
        if (!elm) {
            $.blockUI();
        } else {
            abp.ui.icon = abp.ui.icon || $("#spinner").clone();
            $(elm).block({
                message: abp.ui.icon,
                css: {
                    textAlign: 'center'
                }
            });
        }
    };

    abp.ui.unblock = function (elm) {
        if (!elm) {
            $.unblockUI();
        } else {
            $(elm).unblock();
        }
    };

})();