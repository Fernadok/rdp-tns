﻿var App = App || {};
(function () {

    var appLocalizationSource = abp.localization.getSource('RDP');
    App.localize = function () {
        return appLocalizationSource.apply(this, arguments);
    };

    $(function () {

        /*
         * Link prevent
         */
        $('body').on('click', '.a-prevent', function (e) {
            e.preventDefault();
        });


        /*
         * Tooltips
         */
        if ($('[data-toggle="tooltip"]')[0]) {
            $('[data-toggle="tooltip"]').tooltip();
        }

        /*
         * Popover
         */
        if ($('[data-toggle="popover"]')[0]) {
            $('[data-toggle="popover"]').popover();
        }

        /*
         * IE 9 Placeholder
         */
        if ($('html').hasClass('ie9')) {
            $('input, textarea').placeholder({
                customClass: 'ie9-placeholder'
            });
        }
    });

})(App);


// -- Add function for remove element of Array
(function () {
    if (!Array.prototype.remove) {
        Array.prototype.remove = function (vals, all) {
            var i, removedItems = [];
            if (!Array.isArray(vals)) vals = [vals];
            for (var j = 0; j < vals.length; j++) {
                if (all) {
                    for (i = this.length; i--;) {
                        if (this[i] === vals[j]) removedItems.push(this.splice(i, 1));
                    }
                }
                else {
                    i = this.indexOf(vals[j]);
                    if (i > -1) removedItems.push(this.splice(i, 1));
                }
            }
            return removedItems;
        };
    }
})();

// -- Fix StartsWith, EndWith, IndexOf not support for IE11
(function () {
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function (searchString, position) {
            var subjectString = this.toString();
            if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
                position = subjectString.length;
            }
            position -= searchString.length;
            var lastIndex = subjectString.indexOf(searchString, position);
            return lastIndex !== -1 && lastIndex === position;
        };
    }

    if (!String.prototype.trim) {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        };
    }

    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
            "use strict";
            if (this === null) {
                throw new TypeError();
            }
            var t = Object(this);
            var len = t.length >>> 0;

            if (len === 0) {
                return -1;
            }
            var n = 0;
            if (arguments.length > 1) {
                n = Number(arguments[1]);
                if (n != n) { // shortcut for verifying if it's NaN
                    n = 0;
                } else if (n !== 0 && n != Infinity && n != -Infinity) {
                    n = (n > 0 || -1) * Math.floor(Math.abs(n));
                }
            }
            if (n >= len) {
                return -1;
            }
            var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
            for (; k < len; k++) {
                if (k in t && t[k] === searchElement) {
                    return k;
                }
            }
            return -1;
        };
    }
})();

// -- Re-Write console for isDebug
(function () {
    var isDebud = true;
    // -- console.log
    if (window.console && console.log) {
        var oldlog = console.log;
        console.log = function () {
            if (typeof isDebug != "undefined") {
                Array.prototype.unshift.call(arguments, 'RDP | Debug: ');
                oldlog.apply(this, arguments);
            }
        }
    }

    // -- console.warn
    if (window.console && console.warn) {
        var oldwarn = console.warn;
        console.warn = function () {
            if (typeof isDebug != "undefined") {
                Array.prototype.unshift.call(arguments, 'RDP | Debug: ');
                oldwarn.apply(this, arguments);
            }
        }
    }

    // -- console.error
    if (window.console && console.error) {
        var olderror = console.error;
        console.error = function () {
            if (typeof isDebug != "undefined") {
                Array.prototype.unshift.call(arguments, 'RDP | Debug: ');
                olderror.apply(this, arguments);
                this.trace();
            }
        }
    }
})();

// -- Truncate
(function () {
    String.prototype.trunc =
        function (n, useWordBoundary) {
            var isTooLong = this.length > n,
                s_ = isTooLong ? this.substr(0, n - 1) : this;
            s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
            return isTooLong ? s_ + '...' : s_.replace('"', '');
        };
})();
