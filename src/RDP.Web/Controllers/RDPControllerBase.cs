﻿using Abp.IdentityFramework;
using Abp.UI;
using Abp.Web.Authorization;
using Abp.Web.Localization;
using Abp.Web.Mvc.Controllers;
using Abp.Web.Sessions;
using Abp.Web.Settings;
using Abp.WebApi.Controllers.Dynamic.Scripting;
using Microsoft.AspNet.Identity;
using RDP.Providers;

namespace RDP.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class RDPControllerBase : AbpController
    {
        public IAuthorizationScriptManager AuthorizationScriptManager { get; set; }
        public ILocalizationScriptManager LocalizationScriptManager { get; set; }
        public ScriptProxyManager ScriptProxyManager { get; set; }
        public ISettingScriptManager SettingScriptManager { get; set; }
        public ISessionScriptManager SessionScriptManager { get; set; }
        public INetWorkProvider NetWorkProvider { get; set; }

        protected RDPControllerBase()
        {
            LocalizationSourceName = RDPConsts.LocalizationSourceName;
        }

        protected virtual void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                throw new UserFriendlyException(L("FormIsNotValidMessage"));
            }
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}