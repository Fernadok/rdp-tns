﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Web.Mvc.Authorization;
using Abp.WebApi.Controllers.Dynamic.Scripting;
using Microsoft.Ajax.Utilities;

namespace RDP.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : RDPControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }

        [Route("app/jsproxy")]
        [DisableAuditing]
        public async Task<ActionResult> GetProxy(string types = "")
        {
            try
            {
                var sb = new StringBuilder();

                sb.AppendLine(LocalizationScriptManager.GetScript());
                sb.AppendLine();

                if (AbpSession.UserId.HasValue && AbpSession.UserId.Value != 0)
                {
                    try
                    {
                        sb.AppendLine(SessionScriptManager.GetScript());
                        sb.AppendLine();

                        sb.AppendLine(await AuthorizationScriptManager.GetScriptAsync());
                        sb.AppendLine();

                    }
                    catch (Exception)
                    {
                        return Redirect("/logout");
                    }
                }
                sb.AppendLine(await SettingScriptManager.GetScriptAsync());
                sb.AppendLine();

                if (types == "" || types.ToLower().Contains("jquery"))
                {
                    sb.AppendLine(ScriptProxyManager.GetAllScript(ProxyScriptType.JQuery));
                    sb.AppendLine();
                }
                if (types == "" || types.ToLower().Contains("angular"))
                {
                    sb.AppendLine(ScriptProxyManager.GetAllScript(ProxyScriptType.Angular));
                    sb.AppendLine();
                }

                Minifier minifier = new Minifier();
                var codeSettings = new CodeSettings()
                {
                    EvalTreatment = EvalTreatment.MakeImmediateSafe,
                    PreserveImportantComments = false,
                    MinifyCode = true
                };
              
                string str = minifier.MinifyJavaScript(sb.ToString(), codeSettings);

                return JavaScript(str);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error inesperado ", ex);
            }
        }

    }
}