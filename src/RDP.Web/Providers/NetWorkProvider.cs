﻿using Abp.Dependency;
using RDP.Providers;
using System.Configuration;
using System.IO;
using System.Web;

namespace RDP.Web.Providers
{
    public class NetWorkProvider : INetWorkProvider, ISingletonDependency
    {
        public string GetAppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public bool IsProductionEnv()
        {
            return ConfigurationManager.AppSettings["Environment"].ToString() == "prd";
        }

        public string GetConnetionString()
        {
            return ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        }

        public string MapPath(string path)
        {
            if (path.StartsWith("http")) return "";

            if (HttpContext.Current == null)
            {
                var GetDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                GetDirectory = string.Concat(GetDirectory.Replace("\\bin", ""), path.Replace("/", "\\"));

                return GetDirectory.Replace("file:\\", "");
            }
            return HttpContext.Current.Server.MapPath(path);
        }
    }
}