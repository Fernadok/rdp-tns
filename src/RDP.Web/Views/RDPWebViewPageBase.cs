﻿using Abp.Web.Mvc.Views;

namespace RDP.Web.Views
{
    public abstract class RDPWebViewPageBase : RDPWebViewPageBase<dynamic>
    {

    }

    public abstract class RDPWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected RDPWebViewPageBase()
        {
            LocalizationSourceName = RDPConsts.LocalizationSourceName;
        }
    }
}