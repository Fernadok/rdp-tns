﻿(function () {
    angular
        .module('app')
        .component('orderWizard', {
            transclude  : true,
            templateUrl : "/App/components/wizard/wizard.component.cshtml",
            bindings    : {},
            controller  : WizardComponent,
            controllerAs: 'vm'
        });

    WizardComponent.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        '$timeout',
        '$uibModal',
        'cartService',
        'appSession',
        'productFactory',
        'orderFactory'];

    function WizardComponent($rootScope, $scope, $state, $timeout, $uibModal, cartService, appSession, productService, orderFactory) {
        var vm = this;

        /*** Variables/Objects ***/
        $rootScope.summaryErrors = [];                               // Array de errores reportados por S&C.
        var userConfig          = appSession.user.configurations;    // Toma config según usuario.
        vm.columns              = userConfig.gridProduct.columns;    // Formato que tendra la grilla de productos.
        vm.viewShippingCharge   = userConfig.views.shippingCharge;   // Vista que se va a cargar para el tab Shipping & Charges.
        vm.viewPlaceOrder       = userConfig.views.placeOrder;       // Vista que se va a cargar para el tab Place Order.

        vm.cartItems            = cartService.items.length;          // Variable de item en carrito.
        vm.dataSource           = [];                                // Array de registrso de productos.
        vm.messageErrors        = [];
        vm.nextStep             = 0;
        vm.filters              = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Id',
            order: 'asc',
            itemsTotal: 0
        };
        /*** Variables/Objects ***/

        vm.viewCart             = viewCart;                          // Abre el modal del carrito.
        vm.validateView         = validateView;                      // Metodo que valída los campos requeridos en 'Shipping & Charges'.   
        vm.cancel               = cancel;                            // Cancela la orden.
        vm.save                 = save;                              // Genera la orden.


        $rootScope.$on("deleteItemCart", function (evt, data) { 
            cartService.items.remove(data);
            vm.cartItems = cartService.items.length;
        });

        $rootScope.$on("addToCart", function (evt, data) {           // Evento que se ejecuta al agregar un item al carrito desade el tab productos.
            // Aplicar filter
            var updated = false;
            var prodItem = angular.copy(data);
            $.each(cartService.items, function (inx, item) {
                if (item.id === prodItem.id) {
                    item.quantity += data.quantity;
                    updated = true;
                }
            });

            if (updated) return;
            cartService.items.push(prodItem);
            vm.cartItems = cartService.items.length;
        });

        getProduct()

        /*** Implentations ***/
        this.$onInit = function () {

        };
      
        function getProduct() {
            abp.ui.setBusy(
                $('.table-hover'),
                productService.getAll(vm.filters).then(function (response) {
                    vm.dataSource = response.data;
                })
            );
        }

        function viewCart() {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/modals/cart.modal.cshtml',
                controller: 'cartController as vm',
                backdrop: 'static'
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () { });
        };

        function cancel() {
            abp.message.confirm(
                "Desea descratar el pedido?", App.localize("Atention"),
                function (result) {
                    if (result) {
                        cartService.items = [];
                        $state.go('orders');
                    }
                });
        };

        function save() {
            if (validateView()) {

                var orderData = {
                    orderStatus:1,
                    customerId  :null,
                    orderDetails:[]
                };

                $.each(cartService.items, function (inx,item) {
                    orderData.orderDetails.push({
                        name: item.name,
                        description: item.description,
                        model: item.model,
                        quantity: item.quantity
                    });
                });

                orderFactory.save(orderData).then(function (result) {
                    cartService.items = [];

                    abp.message.confirm(
                        "Se generó la orden no: 0000" + result, App.localize("Information"),
                        function (result) {
                            location.reload();
                        });
                });    
            };
        };
        /*** End Implentations ***/

        function validateView() {
            $rootScope.$emit("formValidate",'');

            if ($rootScope.summaryErrors.length > 0) {
                vm.messageErrors = $rootScope.summaryErrors;
                return false;
            };
            return true;
        };

        /*** WIZARD ***/
        $timeout(function () {
            $(document).ready(function () {
                $.AdminBSB.input.activate();
                //Initialize tooltips
                $('.nav-tabs > li a[title]').tooltip();

                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                    var $target = $(e.target);
                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });


                $(".next-step").click(function (e) {
                    $scope.$apply(function () {
                        if (vm.nextStep < 2) {
                            vm.nextStep++;
                            vm.placeOrder = (vm.nextStep === 2);
                        };
                    })
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);
                })


                $(".prev-step").click(function (e) {
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                });
            });

            function nextTab(elem) {
                $(elem).next().find('a[data-toggle="tab"]').click();
            }
            function prevTab(elem) {
                $(elem).prev().find('a[data-toggle="tab"]').click();
            }
        }, 500);
        /*** FIN WIZARD ***/

    }

})();