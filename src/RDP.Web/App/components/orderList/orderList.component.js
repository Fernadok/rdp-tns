﻿(function () {
    angular
        .module('app')
        .component('orderList', {
            transclude: true,
            templateUrl: "/App/components/orderList/orderList.component.cshtml",
            bindings: {},
            controller: OrderListComponent,
            controllerAs: 'vm'
        });

    OrderListComponent.$inject = ['$rootScope','$state','appSession','$timeout','orderFactory'];

    function OrderListComponent($rootScope, $state, appSession,$timeout,orderFactory) {
        var vm = this;

        /*** Variables/Objects ***/
        var fileConfig  = appSession.customerConfig;          // Recuperamos la configuración de la grilla según customer.

        vm.columns = fileConfig.gridOrderDB.columns;          // Objeto en el que se carga el listado de las ordenes.
        vm.filters      = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Id',
            order: 'asc',
            itemsTotal: 0
        };                                 // Filtros para el servicio de ordenes.
        /*** Variables/Objects ***/

        $timeout(function () {
            getAll();
        }, 500);


        // Evento que redirecciona a vista detalle de una orden.
        $rootScope.$on("viewOrder", function (evt, data) {         
            // Otro caso de como llamar al state con parametros => ui-sref="orderDetail({paramId:data.id})
            $state.go("orderDetail", { paramId: data.orderId });
        });

        /*** Implentations ***/
        function getAll() {
            abp.ui.setBusy(
                $('.table-hover'),
                orderFactory.getAll(vm.filters).then(function (result) {
                    vm.dataSource = result;
                })
            );
        }
        /*** End Implentations ***/
    }
})();