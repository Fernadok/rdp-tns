﻿
(function () {
    angular
        .module('app')
        .component('orderDetail', {
            transclude: true,
            templateUrl: "/App/components/orderDetail/orderDetail.component.cshtml",
            bindings: {},
            controller: OrderDetailComponent,
            controllerAs: 'vm'
        });

    OrderDetailComponent.$inject = ['$stateParams','$timeout', 'orderFactory'];

    function OrderDetailComponent($stateParams,$timeout, orderFactory) {
        var vm = this;
        vm.id = $stateParams.paramId;

        /*** Variables/Objects ***/

        /*** Variables/Objects ***/


        /*** Implentations ***/

        /*** End Implentations ***/


        /*** Form Validate ***/

        /*** End Form Validate ***/

        orderFactory.getById(vm.id).then(function (result) {
            vm.order = result;
        });

        $timeout(function () {
            $(document).ready(function () {
                $.AdminBSB.input.activate();
            })
        }, 500);
    }
})();