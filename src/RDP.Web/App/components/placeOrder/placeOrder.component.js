﻿
(function () {
    angular
        .module('app')
        .component('placeOrder', {
            transclude: true,
            templateUrl: "/App/components/placeOrder/placeOrder.component.cshtml",
            bindings: {},
            controller: PlaceOrderComponent,
            controllerAs: 'vm'
        });

    PlaceOrderComponent.$inject = ['$scope', '$timeout', 'orderFactory'];

    function PlaceOrderComponent($scope, $timeout, orderFactory) {
        var vm = this;
        vm.model = orderFactory.baseModel;          // Carga el modelo de orden de Shipping & Charge.

        /*** Variables/Objects ***/

        /*** Variables/Objects ***/


        /*** Implentations ***/

        /*** End Implentations ***/


        /*** Form Validate ***/

        /*** End Form Validate ***/


        $timeout(function () {
            $(document).ready(function () {
                $.AdminBSB.input.activate();
            })
        }, 500)

    }
})();