﻿(function () {
    angular
        .module('app')
        .component('orderDemo', {
            transclude: true,
            templateUrl: "/App/components/orderDemo/orderDemo.component.cshtml",
            bindings: {},
            controller: OrderDemoComponent,
            controllerAs: 'vm'
        });

    OrderDemoComponent.$inject = ['$scope', '$timeout', '$uibModal', 'appSession', 'orderFactory'];

    function OrderDemoComponent($scope, $timeout, $uibModal, appSession, orderFactory) {
        var vm = this;

        var fileConfig = appSession.customerConfig;     // Recuperamos la configuración de la grilla según customer.

        /*** Variables/Objects ***/
        vm.dataSource = [];

        vm.customer = fileConfig.model.name;
        vm.columns  = fileConfig.gridOrders;
        /*** Variables/Objects ***/

        $timeout(function () {
            GetAll(0);
        }, 500);

        /*** Implentations ***/
        function GetAll() {
            abp.ui.setBusy(
                $('.table-hover'),
                vm.resolve = orderFactory.getByCustomer(appSession.user.userName).then(function (response) {
                    vm.dataSource = response;
                })
            );
        }
        /*** End Implentations ***/
    }
})();