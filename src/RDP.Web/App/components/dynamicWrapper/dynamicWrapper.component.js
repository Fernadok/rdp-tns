﻿(function () {
    angular
        .module('app')
        .component('dynamicWrapper', {
            bindings: {
                name: '@',
                payload: '=?'
            },
            controller: DynamicWrapperComponent,
        });

    DynamicWrapperComponent.$inject = ['$scope', '$compile', '$element'];

    function DynamicWrapperComponent($scope, $compile, $element) {
        var self = this;

        self.$onInit = function () {
            renderComponent(self.name, self.payload);
        };
        function renderComponent(name, payload) {
            var template = '<' + name;

            if (payload) {
                $scope.payload = payload;
                template += ' payload="payload"';
            }

            template += '></' + name + '>';
            $element.append($compile(template)($scope));
        }
    };

})();