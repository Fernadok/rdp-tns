﻿(function () {
    angular
        .module('app')
        .component('gridGeneric', {
            transclude: true,
            templateUrl: "/App/components/gridGeneric/gridGeneric.component.cshtml",
            bindings: {
                columns: '<',
                datasrc: '<',
                metadata: '<'
            },
            controller: GridComponent,
            controllerAs: 'vm'
        });

    GridComponent.$inject = ['$rootScope'];

    function GridComponent($rootScope) {
        var vm = this;

        /*** Variables/Objects ***/
        vm.page = 1;
        /*** Variables/Objects ***/

        this.$onInit = function () {
            vm.columns  = this.columns;
            vm.datasrc  = this.datasrc;
            vm.metadata = this.metadata;
        };

        vm.onClick = function (evt,obj) {
            $rootScope.$emit(evt, obj);
        }

        vm.pageChanged = pageChanged            // Metodo que se ejecutar al realizar el cambio de página.

        /*** Implentations ***/
        function pageChanged() {
            // code..
        }
        /*** End Implentations ***/

    }
})();