﻿(function () {
    angular
        .module('app')
        .component('orderRoot', {
            transclude: true,
            templateUrl: "/App/components/orderRoot/index.component.cshtml",
            bindings: {},
            controller: OrderRootComponent,
            controllerAs: 'vm'
        });

    OrderRootComponent.$inject = [];

    function OrderRootComponent() {
        var vm = this;

        /*** Variables/Objects ***/

        /*** Variables/Objects ***/


        /*** Implentations ***/
  
        /*** End Implentations ***/
    }
})();