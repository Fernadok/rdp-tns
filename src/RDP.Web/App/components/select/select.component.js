﻿(function () {
    angular
        .module('app')
        .component('customSelect', {
            transclude: true,
            templateUrl: "/App/components/select/select.component.cshtml",
            bindings: {
                title: "<",
                list: "<",
                selected: "="
            },
            controller: SelectComponent,
            controllerAs: 'vm'
        });

    SelectComponent.$inject = [];

    function SelectComponent() {
        var vm = this;

        /*** Variables/Objects ***/

        /*** Variables/Objects ***/


        this.$onInit = function () {

        };

        /*** Implentations ***/

        /*** End Implentations ***/

    }
})();