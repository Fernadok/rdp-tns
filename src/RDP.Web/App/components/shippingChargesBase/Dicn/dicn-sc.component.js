﻿
(function () {
    angular
        .module('app')
        .component('dicnSc', {
            transclude: true,
            templateUrl: "/App/components/shippingChargesBase/dicn/dicn-sc.component.cshtml",
            bindings: { payload: '<' },
            controller: DicnSCComponent,
            controllerAs: 'vm'
        });

    DicnSCComponent.$inject = ['$timeout','masterDataFactory', 'orderFactory'];

    function DicnSCComponent($timeout, masterDataFactory, orderFactory) {
        var vm = this;

        /*** Variables/Objects ***/
        vm.model = orderFactory.baseModel;
        vm.model.addressId = 0;
        vm.model.dispatcherId = 0;
        /*** Variables/Objects ***/


        getAddress();                       //Obtiene la lista de Address.
        getDispatchers();                   //Obtiene la lista de Dispatcher.


        /*** Implentations ***/
        function getAddress() {
            vm.address = masterDataFactory.getAddress();
        }
        function getDispatchers() {
            vm.dispatchers = masterDataFactory.getDispatchers();
        }
        /*** End Implentations ***/


        /*** Form Validate ***/

        /*** End Form Validate ***/

    }
})();