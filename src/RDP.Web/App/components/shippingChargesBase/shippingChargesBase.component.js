﻿
(function () {
    angular
        .module('app')
        .component('shippingCharges', {
            transclude: true,
            templateUrl: "/App/components/shippingChargesBase/shippingChargesBase.component.cshtml",
            bindings: {},
            controller: ShippingChargesComponent,
            controllerAs: 'vm'
        });

    ShippingChargesComponent.$inject = ['$rootScope','appSession', '$timeout','masterDataFactory', 'orderFactory'];

    function ShippingChargesComponent($rootScope, appSession, $timeout, masterDataFactory, orderFactory) {
        var vm = this;

        /*** Variables/Objects ***/
        var bu = appSession.user.userName.toLowerCase();

        vm.model = orderFactory.baseModel;
        vm.model.rigId = 0;

        vm.rigs = [];
        vm.wells = [];
        vm.transports = [];
        vm.vehicles = [];

        vm.dynamicComponent = bu + "-sc";                       // Cargo el componente según BU.
        /*** Variables/Objects ***/


        $rootScope.$on('formValidate', function (evt, data) {   // Evento que se ejecutado desee la vista principal para valídar los datos requeridos.
            formValidate();
        });


        getRigs();
        getWells();
        getTransports();
        getVehicles();


        /*** Implentations ***/
        function getRigs() {
            vm.rigs = masterDataFactory.getRigs();
        }
        function getWells() {
            vm.wells = masterDataFactory.getWells();
        }
        function getTransports() {
            vm.transports = masterDataFactory.getTransports();
        }
        function getVehicles() {
            vm.vehicles = masterDataFactory.getVehicles();
        }


        function formValidate() {
            $rootScope.summaryErrors = [];
            if (vm.model.name === '') {
                $rootScope.summaryErrors.push("Nombre es requerido.");
            }
            if (vm.model.surname === '') {
                $rootScope.summaryErrors.push("Apelliodo es requerido.");
            }
            if (vm.model.email === '') {
                $rootScope.summaryErrors.push("Email es requerido.");
            }
            if (vm.model.description === '') {
                $rootScope.summaryErrors.push("Descrripción es requerido.");
            }
            if (vm.model.rigId === 0) {
                $rootScope.summaryErrors.push("Rig es requerido.");
            }

            if (bu === 'dicn') {
                ValidateComponentDICN();
            }         
        }       
        /*** End Implentations ***/


        /*** Form Validate ***/
        function ValidateComponentDICN() {
            if (vm.model.addressId === 0) {
                $rootScope.summaryErrors.push("Address es requerido.");
            }
            if (vm.model.dispatcherId === 0) {
                $rootScope.summaryErrors.push("Dispatcher es requerido.");
            }
        };
        /*** End Form Validate ***/

    }
})();