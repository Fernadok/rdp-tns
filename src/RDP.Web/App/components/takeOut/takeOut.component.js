﻿(function () {
    angular
        .module('app')
        .component('takeout', {
            transclude: true,
            templateUrl: "/App/components/takeOut/takeOu.component.cshtml",
            bindings: {},
            controller: TakeOutComponent,
            controllerAs: 'vm'
        });

    TakeOutComponent.$inject = [];

    function TakeOutComponent() {
        var vm = this;

        /*** Variables/Objects ***/

        /*** Variables/Objects ***/


        this.$onInit = function () {

        };


        /*** Implentations ***/
   
        /*** End Implentations ***/

    }
})();