﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',
        'ngCookies',
        'ui.router',
        'ui.bootstrap',
        'ui.jq',
        'cfp.loadingBar',
        'oc.lazyLoad',
        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider', '$ocLazyLoadProvider', 'cfpLoadingBarProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $qProvider, $ocLazyLoadProvider, cfpLoadingBarProvider) {
            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);

            // -- Config Loading Bar
            cfpLoadingBarProvider.includeSpinner = true;
            cfpLoadingBarProvider.latencyThreshold = 500;

            // -- Config $ocLazyLoadProvider
            $ocLazyLoadProvider.config({
                modules: [{
                    name: 'BaseJs',
                    files: [
                        'App/components/select/select.component.js',
                        'App/components/dynamicWrapper/dynamicWrapper.component.js',
                        'App/components/takeOut/takeOut.component.js',
                        'App/components/shippingChargesBase/shippingChargesBase.component.js',
                        'App/components/shippingChargesBase/dicn/dicn-sc.component.js',
                        'App/components/placeOrder/placeOrder.component.js',
                        'App/components/gridGeneric/gridGeneric.component.js',
                        'App/components/wizard/wizard.component.js'
                    ]
                }]
            });

            if (abp.auth.hasPermission('Pages.Orders')) {
                $stateProvider
                    // Vista parent de ordenes.
                    .state('orders', {
                        url: '/orders',
                        template: '<order-root></order-root>',
                        menu: 'Orders',
                        resolve: {
                            loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'App/components/orderRoot/index.component.js'
                                ]);
                            }]
                        }
                    })

                    // Vista de ordenes en DB.
                    .state('orders.orderdb', {
                        url: '/orderd-desde-base',
                        menu: 'Orderdb',
                        views: {
                            'order': {
                                template: '<order-list></order-list>',
                            }
                        },
                        resolve: {
                            loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'App/components/orderList/orderList.component.js'
                                ]);
                            }]
                        }
                    })

                    // Vista de creación de orden.
                    .state('orders.takeOut', {
                        url: '/takeOut',
                        menu: 'TakeOutt',
                        views: {
                            'order': {
                                template: '<takeout></takeout>'
                            }
                        },
                        resolve: {
                            loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load('BaseJs');
                            }]
                        }
                    })

                    // Vista detalle de orden.
                    .state('orderDetail', {
                        url: '/order/:paramId/detail',
                        template: '<order-detail></order-detail>',
                        menu: 'OrderDetail',
                        resolve: {
                            loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'App/components/orderDetail/orderDetail.component.js'
                                ]);
                            }]
                        }
                    })

                    // Vista de ordenes (API Placeholder).
                    .state('orders.list', {
                        url: '/list',
                        menu: 'Orders List',
                        views: {
                            'order': {
                                template: '<order-demo></order-demo>',
                            }
                        },
                        resolve: {
                            loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'App/components/orderDemo/orderDemo.component.js'
                                ]);
                            }]
                        }
                    })

                    // Demo vista que es cargada en distintas ui-view.
                    .state('ordersList', {
                        url: '/list',
                        template: '<order-demo></order-demo>',
                    });
            }

            if (abp.auth.hasPermission('Pages.Users')) {
                $stateProvider
                    .state('users', {
                        url: '/users',
                        templateUrl: '/App/Main/views/users/index.cshtml',
                        menu: 'Users'
                    });
            }

            if (abp.auth.hasPermission('Pages.Roles')) {
                $stateProvider
                    .state('roles', {
                        url: '/roles',
                        templateUrl: '/App/Main/views/roles/index.cshtml',
                        menu: 'Roles'
                    });
            }

            if (abp.auth.hasPermission('Pages.Products')) {
                $stateProvider
                    .state('products', {
                        url: '/products',
                        templateUrl: '/App/Main/views/products/index.cshtml',
                        menu: 'Products'
                    });
            }

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load('BaseJs');
                        }]
                    }
                });
        }
    ]).run(['$rootScope', 'routeDynamic', '$timeout', '$location', '$window', '$state', 'cfpLoadingBar',
        function ($rootScope, routeDynamic, $timeout, $location, $window, $state, cfpLoadingBar) {

            routeDynamic.SetStateDynamic();

            var openLoading = function () {
                $('.page-loader').fadeIn();
            };

            var closedLoading = function () {
                $timeout(function () {
                    $('.page-loader').slideUp();
                }, 500);
            };

            var closedLeft = function () {
                $timeout(function () {
                    var isOpen = $("body.ls-closed").hasClass("overlay-open");
                    if (isOpen) {
                        $(".bars").click();
                    }
                }, 200);
            };

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                // openLoading();
            });
            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                document.title = "RDP | " + toState.menu;
                closedLoading();
                closedLeft();
                cfpLoadingBar.complete();
            });
            $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams) { });
            $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) { });
            $rootScope.$on('$viewContentLoading', function (event, viewConfig) { });
            $rootScope.$on('$viewContentLoaded', function (event, viewConfig) { });

        }]);

})();