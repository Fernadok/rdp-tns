﻿(function () {
    angular
        .module('app')
        .controller('cartController', CartController);

    CartController.$inject = ['$uibModalInstance', 'cartService','appSession'];

    function CartController($uibModalInstance, cartService, appSession) {
        var vm = this;

        /*** Variables/Objects ***/
        vm.columns = appSession.user.configurations.gridCart.columns;
        vm.items   = cartService.items;      // Item seleccionados de la grilla de prodictos.
        /*** Variables/Objects ***/

        vm.save    = save;                   // Metodo para guardar.
        vm.cancel  = cancel;                 // Cierra el modal.

        /*** Implentations ***/
        function save() {
            $uibModalInstance.close();
        };

        function cancel() {
            $uibModalInstance.dismiss({});
        };
        /*** End Implentations ***/
    }
})();