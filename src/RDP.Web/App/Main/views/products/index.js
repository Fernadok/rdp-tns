﻿(function () {
    angular
        .module('app')
        .controller('productController', ProductController);

    ProductController.$inject = ['$scope', '$uibModal', 'productFactory'];

    function ProductController($scope, $uibModal, productFactory) {
        var vm = this;

        vm.filters = {
            search: '',
            page: 1,
            pageSize: 10,
            orderColumn: 'Name',
            order: 'asc',
            itemsTotal: 0
        };

        vm.dataContext = [];

        vm.addOrUpdateModalModal = addOrUpdateModalModal;           // Modal para cargar/editar producto.
        vm.deleteProduct         = deleteProduct;                   // Elimina un producto.
        vm.refresh               = refresh;                         // Refresca la grilla.
        vm.pageChanged           = pageChanged;

        getProducts();

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function addOrUpdateModalModal(p) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/products/addOrUpdate.modal.cshtml',
                controller: 'productModalController as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return p ? p.id : null;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getProducts();
            });
        };

        function deleteProduct(id) {
            abp.message.confirm(
                "Delete product '" + id + "'?", App.localize("Atention"),
                function (result) {
                    if (result) {
                        productFactory.clean(id).then(function () {
                            abp.notify.info("Deleted product: " + id);
                            getProducts();
                        });
                    }
                });
        }

        function refresh() {
            getProducts();
        };

        function getProducts() {
            abp.ui.setBusy(
                $('.table-hover'),
                productFactory.getAll(vm.filters).then(function (result) {
                    vm.dataContext = result.data;
                })
            );
        }

        $scope.$watch("vm.filters.search", function () {
            vm.filters.page = 1;
            getProducts();
        });

        function pageChanged() {
            getProducts();
        };
        /*** End Implentations ***/

    }
})();