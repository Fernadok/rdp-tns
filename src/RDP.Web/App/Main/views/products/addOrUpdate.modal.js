﻿(function () {
    angular
        .module('app')
        .controller('productModalController', ProductModalController);

    ProductModalController.$inject = ['$uibModalInstance','productFactory','id'];

    function ProductModalController($uibModalInstance, productFactory, id) {
        var vm = this;

        vm.permission = {
            hasEdit: abp.auth.hasPermission("Pages.Products")   // Trae los permmisos para editar.
        }

        vm.isNew = (id === null);
        vm.product = {};

        vm.cancel   = cancel;
        vm.save     = save;

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function cancel() {
            $uibModalInstance.dismiss({});
        };

        function save() {
            abp.ui.setBusy(".modal-content");
            productFactory.save(vm.product).then(function () {
                abp.ui.clearBusy(".modal-content");
                $uibModalInstance.close(true);
            }, function (error) {
                abp.ui.clearBusy(".modal-content");
            });
        }

        if (!vm.isNew) {
            productFactory.getById(id).then(function (result) {
                vm.product = result.data;
            });
        }
        /*** End Implentations ***/
    }
})();