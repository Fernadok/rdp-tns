﻿(function () {
    angular
        .module('app')
        .controller('userController', UserController);

    UserController.$inject = ['$scope', '$timeout', '$uibModal', 'abp.services.app.user'];

    function UserController($scope, $timeout, $uibModal, userService) {
        var vm = this;

        vm.users = [];
      
        vm.openUserCreationModal = openUserCreationModal;
        vm.openUserEditModal     = openUserEditModal;
        vm.delete                = deletesSeer;
        vm.refresh               = refresh;

        getUsers();

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function openUserCreationModal() {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/users/createModal.cshtml',
                controller: 'userCreateController as vm',
                backdrop: 'static'
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getUsers();
            });
        };

        function openUserEditModal(user) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/users/editModal.cshtml',
                controller: 'userEditController as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return user.id;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getUsers();
            });
        };

        function deletesSeer(user) {
            abp.message.confirm(
                "Delete user '" + user.userName + "'?",
                function (result) {
                    if (result) {
                        userService.delete({ id: user.id })
                            .then(function () {
                                abp.notify.info("Deleted user: " + user.userName);
                                getUsers();
                            });
                    }
                });
        }

        function refresh() {
            getUsers();
        };

        function getUsers() {
            abp.ui.setBusy(
                $('.table-responsive'),
                userService.getAll({}).then(function (result) {
                    vm.users = result.data.items;
                })
            );
        }
        /*** End Implentations ***/
    }
})();