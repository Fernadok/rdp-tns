﻿(function () {
    angular
        .module('app')
        .controller('layoutController', LayoutController);

    LayoutController.$inject = ['$rootScope', '$scope', 'appSession', '$timeout'];

    function LayoutController($rootScope, $scope, $appSession, $timeout) {
        var vm = this;

        vm.activateLeftSideBar  = activateLeftSideBar;
        vm.activateRightSideBar = activateRightSideBar;
        vm.activateTopBar       = activateTopBar

        $rootScope.$on('onRefreshMenu', function (event, opt) {
            vm.isClose = $appSession.isCloseMenu();
        });

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function activateLeftSideBar() {
            $timeout(function () {
                $.AdminBSB.leftSideBar.activate();
            }, 2000);
        };

        function activateRightSideBar () {
            $timeout(function () {
                $.AdminBSB.rightSideBar.activate();
            }, 2000);
        };

        function activateTopBar() {
            $.AdminBSB.search.activate();
            $.AdminBSB.navbar.activate();
        };
        /*** End Implentations ***/

    }
})();