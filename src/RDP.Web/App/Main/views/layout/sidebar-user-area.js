﻿(function () {
    angular
        .module('app')
        .controller('layoutController.sidebarUserArea', LayoutSidebarUserAreaController);

    LayoutSidebarUserAreaController.$inject = ['$rootScope', '$state', 'appSession'];

    function LayoutSidebarUserAreaController($rootScope, $state, appSession) {
        var vm = this;

        vm.userEmailAddress = appSession.user.emailAddress;
        vm.getShownUserName = function () {
            return appSession.user.userName;
        };
    }
})();