﻿(function () {
    angular
        .module('app')
        .controller('layoutController.sidebarNav', LayoutSidebarNavController);

    LayoutSidebarNavController.$inject = ['$rootScope', '$state', 'appSession'];

    function LayoutSidebarNavController($rootScope, $state, $appSession) {
        var vm = this;

        vm.isClose = $appSession.isCloseMenu();

        // -- Check if is close menu
        vm.closeOrOpenMenu = function () {
            $appSession.closeOrOpenMenu();
            vm.isClose = $appSession.isCloseMenu();
            $rootScope.$emit('onRefreshMenu');
        };

        vm.menuItems = [
            createMenuItem(App.localize("HomePage"), "", "home", "home"),
            createMenuItem(App.localize("Orders"), "Pages.Orders", "receipt", "orders.list"),
            createMenuItem(App.localize("View Parent"), "Pages.Orders", "receipt", "orders"),
            createMenuItem(App.localize("View Child"), "Pages.Orders", "receipt", "ordersList"),
            createMenuItem(App.localize("Products"), "Pages.Products", "store", "products"), 
            createMenuItem(App.localize("Users"), "Pages.Users", "people", "users"),
            createMenuItem(App.localize("Roles"), "Pages.Roles", "local_offer", "roles")
        ];

        vm.showMenuItem = function (menuItem) {
            if (menuItem.permissionName) {
                return abp.auth.isGranted(menuItem.permissionName);
            }
            return true;
        }

        function createMenuItem(name, permissionName, icon, route, childItems) {
            return {
                name: name,
                permissionName: permissionName,
                icon: icon,
                route: route,
                items: childItems
            };
        }
    }
})();