﻿(function () {
    angular
        .module('app')
        .controller('rolesController', RolesController);

    RolesController.$inject = ['$scope', '$uibModal', 'abp.services.app.role'];

    function RolesController($scope, $uibModal, roleService) {
        var vm = this;

        vm.roles = [];

        vm.openRoleCreationModal = openRoleCreationModal;
        vm.openRoleEditModal     = openRoleEditModal;
        vm.delete                = deleteRole;
        vm.refresh               = refresh;

        getRoles();

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function openRoleCreationModal() {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/roles/createModal.cshtml',
                controller: 'roleCreateController as vm',
                backdrop: 'static'
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getRoles();
            });
        };

        function openRoleEditModal(role) {
            var modalInstance = $uibModal.open({
                templateUrl: '/App/Main/views/roles/editModal.cshtml',
                controller: 'roleEditController as vm',
                backdrop: 'static',
                resolve: {
                    id: function () {
                        return role.id;
                    }
                }
            });

            modalInstance.rendered.then(function () {
                $.AdminBSB.input.activate();
            });

            modalInstance.result.then(function () {
                getRoles();
            });
        };

        function deleteRole(role) {
            abp.message.confirm(
                "Delete role '" + role.name + "'?",
                function (result) {
                    if (result) {
                        roleService.delete({ id: role.id })
                            .then(function () {
                                abp.notify.info("Deleted role: " + role.name);
                                getRoles();
                            });
                    }
                });
        }

        function refresh() {
            getRoles();
        };

        function getRoles() {
            abp.ui.setBusy(
                $('.table-responsive'),
                roleService.getAll({}).then(function (result) {
                    vm.roles = result.data.items;
                })
            );
        }
        /*** End Implentations ***/

    }
})();