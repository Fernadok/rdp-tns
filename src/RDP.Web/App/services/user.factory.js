﻿(function () {
    angular
        .module('app')
        .factory('userFactory', UserFactory);

    UserFactory.$inject = ['$http'];

    function UserFactory($http) {
        var factory = {
            getById: getById,    // Método que obtiene la información de un usuario.
            getAll : getAll,     // Método para recuperar todos los usuarios.
            insert : insert,     // Método que inserta un usuario.
            update : update,     // Método para actualizar la información de un usuario.
            delete : clean,      // Método para borrar un usuario.
            search : search,     // Buscar usuarios según criterios.
            save   : save        // Guarda un usuario.
        };

        return factory;

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function getById(id) {
            return $http.get("https://jsonplaceholder.typicode.com/posts/" + id).then(function (response) {
                return response.data;
            });
        }

        function getAll() {
            //code..
        }

        function insert(data) {
            //code..
        }

        function update(data) {
            //code..
        }

        function clean(id) {
            //code..
        }

        function search(id) {
            //code..
        }

        function save(data) {
            //code..
        }
        /*** End Implentations ***/
    }

})();