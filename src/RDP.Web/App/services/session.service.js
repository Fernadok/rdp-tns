﻿(function () {
    angular
        .module('app')
        .factory('appSession', AppSession);

    AppSession.$inject = ['$cookieStore','$http'];

    function AppSession($cookieStore, $http) {

        var _session = {
            user            : null,                 // User Information.
            tenant          : null,                 // Tenant Information.
            isCloseMenu     : isClose,              // Get Menu bar status.
            closeOrOpenMenu : closeOrOpenMenu       // Save iin cookies menu status. 
        };

        if (!$cookieStore.get('menu')) {
            $cookieStore.put('menu', 'close');
        }

        getUser();

        return _session;

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function getUser() {
            abp.services.app.session.getCurrentLogin({ async: false }).done(function (result) {
                _session.user = result.user;
                _session.tenant = result.tenant;
                _session.customerConfig = result.user.configurations;
            });
        };

        function isClose () {
            return $cookieStore.get('menu') === 'close'
        };

        function closeOrOpenMenu() {
            if (isClose()) {
                $cookieStore.put('menu', 'open');
            } else {
                $cookieStore.put('menu', 'close');
            }
        };
        /*** End Implentations ***/
    }
})();