﻿
(function () {
    angular
        .module('app')
        .factory('productFactory', ProductFactory);

    ProductFactory.$inject = ['$http', 'abp.services.app.productService'];

    function ProductFactory($http, productService) {
        var factory = {
            getById: getById,     // Método que obtiene la información de un producto.
            getAll : getAll,      // Método para recuperar todos los producto.
            clean  : clean,       // Método para borrar un producto.
            search : search,      // Buscar usuarios según criterios.
            save   : save         // Guarda un producto.
        };

        return factory;

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function getById(id) {
            return productService.getById(id).then(function (result) {
                return result;
            })
        }

        function getAll(filter) {
            return productService.getAll(filter).then(function (result) {
                return result;
            })
        }

        function clean(id) {
            return productService.delete(id).then(function () {
                return true;
            });
        }

        function save(data) {
            return productService.addOrUpdate(data).then(function () {
                return true;
            });
        }
        function search() {
            //code...
        }
        /*** End Implentations ***/
    }

})();