﻿(function () {
    angular
        .module('app')
        .factory('masterDataFactory', MasterDataFactory);

    MasterDataFactory.$inject = ['$http', '$timeout'];

    function MasterDataFactory($http, $timeout) {

        var _session = {
            getRigs         : getRigs,                  // Returna una lista de rigs.
            getWells        : getWells,                 // Returna una lista de wells.
            getTransports   : getTransports,            // Returna una lista de transports.
            getAddress      : getAddress,               // Returna una lista de sddress.
            getDispatchers  : getDispatchers,           // Returna una lista de dispatcvher.
            getVehicles     : getVehicles               // Returna una lista de vehicles.
        };

        return _session;

        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function getRigs() {
    
                return [
                    { id: 1, value: '11', description: 'Rig desc 1' },
                    { id: 2, value: '22', description: 'Rig desc 2' },
                    { id: 3, value: '33', description: 'Rig desc 3' },
                    { id: 4, value: '44', description: 'Rig desc 4' },
                    { id: 5, value: '55', description: 'Rig desc 5' }
                ];
          
        }
        function getWells() {
         
                return [
                    { id: 1, value: '11', description: 'Well desc 1' },
                    { id: 2, value: '22', description: 'Well desc 2' },
                    { id: 3, value: '33', description: 'Well desc 3' },
                    { id: 4, value: '44', description: 'Well desc 4' },
                    { id: 5, value: '55', description: 'Well desc 5' }
                ];
        }
        function getTransports() {
         
                return [
                    { id: 1, value: '11', description: 'Transport desc 1' },
                    { id: 2, value: '22', description: 'Transport desc 2' },
                    { id: 3, value: '33', description: 'Transport desc 3' },
                    { id: 4, value: '44', description: 'Transport desc 4' },
                    { id: 5, value: '55', description: 'Transport desc 5' }
                ];
     
        }
        function getAddress() {
          
                return [
                    { id: 1, value: '11', description: 'Address desc 1' },
                    { id: 2, value: '22', description: 'Address desc 2' },
                    { id: 3, value: '33', description: 'Address desc 3' },
                    { id: 4, value: '44', description: 'Address desc 4' },
                    { id: 5, value: '55', description: 'Address desc 5' }
                ];
     
        }
        function getDispatchers() {
      
                return [
                    { id: 1, value: '11', description: 'Dispatcher desc 1' },
                    { id: 2, value: '22', description: 'Dispatcher desc 2' },
                    { id: 3, value: '33', description: 'Dispatcher desc 3' },
                    { id: 4, value: '44', description: 'Dispatcher desc 4' },
                    { id: 5, value: '55', description: 'Dispatcher desc 5' }
                ];
    
        }
        function getVehicles() {
       
                return [
                    { id: 1, value: '11', description: 'Vehicle desc 1' },
                    { id: 2, value: '22', description: 'Vehicle desc 2' },
                    { id: 3, value: '33', description: 'Vehicle desc 3' },
                    { id: 4, value: '44', description: 'Vehicle desc 4' },
                    { id: 5, value: '55', description: 'Vehicle desc 5' }
                ];
    
        }
        /*** End Implentations ***/
    }
})();