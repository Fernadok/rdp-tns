﻿(function () {
    angular
        .module('app')
        .provider('routeDynamic',
            function ($stateProvider) {
                this.$get = function () {
                    function SetStateDynamic() {
                        abp.services.app.session.getCurrentLogin({ async: false }).done(function (result) {
                            var views = result.user.configurations.views;
                            if (abp.auth.hasPermission('Pages.Orders')) {
                                    $stateProvider
                                        // Vista de creación de orden -> shippingCharge.
                                        .state('orders.takeOut.shippingCharge', {
                                            url: '/shippingCharge',
                                            menu: 'TakeOutt-shippingCharge',
                                            views: {
                                                'shippingCharge': {
                                                    templateUrl: views.shippingCharge
                                                }
                                            }
                                        })

                                        // Vista de creación de orden -> placeOrder.
                                        .state('orders.takeOut.placeOrder', {
                                            url: '/placeOrder',
                                            menu: 'TakeOutt-placeOrder',
                                            views: {
                                                'placeOrder': {
                                                    templateUrl: views.placeOrder
                                                }
                                            }
                                        });
                                }

                        });
                    }
                    return {
                        SetStateDynamic: SetStateDynamic
                    }
                };
            }
        );

})();