﻿(function () {
    angular
        .module('app')
        .factory('orderFactory', OrderFactory);

    OrderFactory.$inject = ['$http', 'abp.services.app.orderService'];

    function OrderFactory($http, orderService) {
        var factory = {
            baseModel    : {},              // Modelo de los dato segúm tipo de BU.
            getByCustomer: getByCustomer,   // Obtiene las ordénes por customer.
            getById      : getById,         // Método que obtiene la información de una orden.
            getAll       : getAll,          // Método para recuperar todos las ordenes.
            insert       : insert,          // Método que inserta una orden.
            update       : update,          // Método para actualizar la información de una orden.
            delete       : clean,           // Método para borrar una orden.
            search       : search,          // Buscar usuarios según criterios.
            save         : save             // Guarda la orden.
        };

        factory.baseModel = {
            name: '',
            surname: '',
            email: '',
            description: ''
        };

        return factory;


        //////////////////////////////////////////////////////////////////
        /*** Implentations ***/
        function getByCustomer(name) {
            var url = "";
            switch (name) {
                case 'admin': url = "https://jsonplaceholder.typicode.com/posts"; break;
                case 'DICN': url = "https://jsonplaceholder.typicode.com/comments"; break;
                case 'DUSS': url = "https://jsonplaceholder.typicode.com/todos"; break;
                case 'DUSN': url = "https://jsonplaceholder.typicode.com/users"; break;
                default:
            }

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getById(id) {
            return orderService.getById(id).then(function (respopnse) {
                return respopnse.data;
            });
        }

        function getAll(filter) {
            return orderService.getAll(filter).then(function (respopnse) {
                return respopnse.data;
            });
        }

        function insert(data) {
            //code..
        }

        function update(data) {
            //code..
        }

        function clean(id) {
            //code..
        }

        function search(id) {
            //code..
        }

        function save(data) {
            return orderService.addOrUpdate(data).then(function (respopnse) {
                return respopnse.data;
            });
        }
        /*** End Implentations ***/
    }

})();