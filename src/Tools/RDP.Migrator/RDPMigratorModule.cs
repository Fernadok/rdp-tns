using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using RDP.EntityFramework;

namespace RDP.Migrator
{
    [DependsOn(typeof(RDPDataModule))]
    public class RDPMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<RDPDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}